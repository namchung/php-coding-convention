# Load tự động

Những từ khóa "PHẢI", "PHẢI KHÔNG", "BẮT BUỘC", "KHÔNG NÊN",
, "GỢI Ý", "CÓ THỂ", và "OPTIONAL"  trong tài liệu này được giải thích
như mô tả trong [RFC 2119].

[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt
[PSR-0]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
[PSR-4]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md


## 1. Tổng quát

PSR này diễn tả một đặc điểm kỹ thuật(specification) cho các class [Load tự động] 
từ đường dẫn file. Nó hoàn toàn tương thích, và có thể được sử dụng thêm vào bất kỳ
đặc điểm kỹ thuật load tự động nào khác, kể cả [PSR-0][]. PSR này cũng miêu tả nơi 
đặt các file sẽ được load tự động vào file đặc điểm kỹ thuật.

## 2. Đặc điểm kỹ thuật (Specification)

1. Thuật ngữ "class" dùng để chỉ các class, interface, các tính trạng và các cấu trúc 
tương tự khác.

2. Một tên class đạt tiêu chuẩn có dạng như sau:

        \<NamespaceName>(\<SubNamespaceNames>)*\<ClassName>

    1. Một tên class đạt tiêu chuẩn PHẢI có tên namespace thư mục ở trên,
       còn được gọi là "vendor namespace".

    2. Một tên class đạt tiêu chuẩn CÓ THỂ có một hoặc nhiều namespace phụ.

    3. Một tên class đạt tiêu chuẩn PHẢI có kết thúc là tên class.

    4. Dấu gạch dưới không có ý nghĩ đặc biệt nào trong bất kỳ phần nảo của
       một tên class đạt tiêu chuẩn.

    5. Các ký tự trong bảng chữ cái trong một tên class đạt tiêu chuẩn CÓ THỂ
       là kết hợp của bất kỳ các ký tự in thường hoặc in hoa.
      
    6. Tất cả các tên class PHẢI được tra cứu riêng biệt dựa theo các ký tụ
       in hoa hoặc in thường.

3. Khi đọc một file có tên class đạt tiêu chuẩn tương ứng...

    1. Một dãy liền kề của một hoặc nhiều namespace và namespace phụ phỉa trước
       không được tính trong dấu phân cách namespace , trong một tên class đạt 
       tiêu chuẩn , phần phía trước namespace bao gồm ít nhất một "thư mục gốc".

    2. Các namespace phụ liền kề sau "phần phía trước namespace" liên quan đến
       một thư mục phụ phía trong "thư mục gốc", trong đó dấu phân cách namespace 
       đại diện cho phân cách thư mục. Tên thư mục phụ PHẢI khớp với tên namespace
       phụ.

    3. Kết thúc tên class phải là một file name kết thúc bằng `.php`.
       Tên file PHẢI khướp với tên kết thúc của class.

4. Thực hiện autoloader PHẢI không sinh ra exception, KHÔNG gây ra lỗi của bất kỳ 
   cấp độ nào, và NÊN KHÔNG trả lại một giá trị.

## 3. Ví dụ

Bảng dưới đây chỉ ra các đường dẫn file liên quan cho một tên class đạt tiêu chuẩn,
tiền tố của namespace và thư mục gốc.

| tên class đạt tiêu chuẩ       | tiền tố của namespace| Thư mục gốc              | Kết quả đường dẫn file
| ----------------------------- |----------------------|--------------------------|-------------------------------------------
| \Acme\Log\Writer\File_Writer  | Acme\Log\Writer      | ./acme-log-writer/lib/   | ./acme-log-writer/lib/File_Writer.php
| \Aura\Web\Response\Status     | Aura\Web             | /path/to/aura-web/src/   | /path/to/aura-web/src/Response/Status.php
| \Symfony\Core\Request         | Symfony\Core         | ./vendor/Symfony/Core/   | ./vendor/Symfony/Core/Request.php
| \Zend\Acl                     | Zend                 | /usr/includes/Zend/      | /usr/includes/Zend/Acl.php

Để xem thêm ví dụ việc triển khai các bộ load tự động phù hợp với đặc điểm kỹ thuật, 
vui lòng xem [ví dụ file] []. Việc triển khai ví dụ không được coi là một phần của đặc
 tả và CÓ THỂ thay đổi bất cứ lúc nào.
[autoloading]: http://php.net/autoload
[PSR-0]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
[ví dụ file]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
