Hướng dẫn Coding Style
==================

Hướng dẫn này mở rộng trên [PSR-1], tiêu chuẩn mã hóa cơ bản.

Mục đích của hướng dẫn này là giảm chênh lệch nhận thức khi đọc code từ các tác giả khác nhau
bằng cách liệt kê một tập hợp các quy tắc được chia sẻ và kỳ vọng về cách định dạng code PHP.

Các quy tắc về phong cách code trong tài liệu này được rút ra từ các tính chất chung giữa các 
dự án thành viên khác nhau. Khi các tác giả khác nhau cộng tác trong nhiều dự án, sẽ giúp có 
một bộ hướng dẫn được sử dụng trong tất cả các dự án đó. Do đó, lợi ích của hướng dẫn này 
không phải là các quy tắc, mà là việc chia sẻ các quy tắc đó.

Những từ khóa "PHẢI", "PHẢI KHÔNG", "BẮT BUỘC", "KHÔNG NÊN",
, "GỢI Ý", "CÓ THỂ", và "OPTIONAL"  trong tài liệu này được giải thích
như mô tả trong [RFC 2119].

[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt
[PSR-0]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
[PSR-1]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md


1. Khái quát chung
-----------
    
- Code Mã PHẢI tuân theo "coding style guide" PSR [[PSR-1]].

- Code PHẢI sử dụng 4 khoảng trống để thụt lề, không phải tab.

- Mỗi dòng code PHẢI dưới 120 ký tự, NÊN dưới 80 ký tự.

- PHẢI có 1 dòng trắng sau namespace, và PHẢI có một dòng trắng sau mỗi khối khai báo `use`.

- Ký tự mở class { PHẢI ở dòng tiếp theo, và đóng lớp } PHẢI ở dòng tiếp theo của thân class.

- Ký tự { cho methods PHẢI ở dòng tiếp theo, và ký tự } kết thúc hàm PHẢI ở dòng tiếp theo của thân hàm.

- Các visibility (public, private, protected) PHẢI được khai báo cho tất cả các hàm và các thuộc tính của class;
  `abstract` và `final`PHẢI được khai báo trước visibility; 
  `static` PHẢI được khai báo sau visibility.

- Các từ khóa điều khiển khối(if, elseif, else) PHẢI có một khoảng trống phía sau;
  method và class thì KHÔNG ĐƯỢC làm như vậy.

- Mở khối { cho cấu trúc điều khiển(control structures) PHẢI trên cùng một dòng;
  và đóng khối này } với ở dòng tiếp theo của thân khối.

- Mở ngoặc ( cho cấu trúc điều khiển KHÔNG ĐƯỢC có một khoảng trống phía sau,
  Và đóng dấu ngoặc đơn cho các cấu trúc điều khiển KHÔNG ĐƯỢC có một khoảng trống phía trước.
 
### 1.1. Ví dụ

~~~php
<?php
namespace Vendor\Package;

use FooInterface;
use BarClass as Bar;
use OtherVendor\OtherPackage\BazClass;

class Foo extends Bar implements FooInterface
{
    public function sampleMethod($a, $b = null)
    {
        if ($a === $b) {
            bar();
        } elseif ($a > $b) {
            $foo->bar($arg1);
        } else {
            BazClass::bar($arg2, $arg3);
        }
    }

    final public static function bar()
    {
        // method body
    }
}
~~~

2. Tổng quát
----------

### 2.1. Tiêu chuẩn mã hóa cơ bản

Code PHẢI tuân thủ tất cả các quy tắc được nêu trong [PSR-1].

### 2.2. Files

Tất cả PHP files PHẢI sử dụng dòng Unix LF (linefeed) kết thúc.

Tất cả  PHP files  PHẢI kết thúc bằng một dòng trống.

Dấu `?>` PHẢI được bỏ qua ở những file chỉ bao gồm PHP

### 2.3. 

Không bắt buộc phải có giới hạn cứng độ dài mỗi dòng.

Giới hạn mềm độ dài mỗi dòng PHẢI là 120 ký tự;
Kiểm tra style tự động PHẢI cảnh báo nhưng KHÔNG PHẢI báo lỗi ở giới hạn mềm.

Các dòng KHÔNG NÊN dài hơn 80 ký tự; dòng dài hơn NÊN Được chia thành nhiều dòng tiếp theo không quá 80 ký tự

KHÔNG CẦN có khoảng trống cuối ở các dòng không trống.

Dòng trống có thể được thêm vào để cải thiện khả năng đọc và để chỉ các khối liên quan của code.

Không thể có nhiều hơn một câu lệnh trên mỗi dòng.

### 2.4. Thụt lề

- Code PHẢI sử dụng 4 khoảng trống để thụt lề, không phải tab.

### 2.5. Keywords and True/False/Null

PHP [keywords]  PHẢI viết với chữ thường.

The PHP constants `true`, `false`, va `null`  PHẢI viết với chữ thường.

[keywords]: http://php.net/manual/en/reserved.keywords.php



3. Namespace and Use Declarations
---------------------------------

When present, there MUST be one blank line after the `namespace` declaration.

When present, all `use` declarations MUST go after the `namespace`
declaration.

There MUST be one `use` keyword per declaration.

There MUST be one blank line after the `use` block.

For example:
Khi sử dụng, PHẢI có một dòng trống sau `namespace'.

Khi sử dụng, tất cả các khai báo `use` PHẢI đi sau` namespace`.

PHẢI là một từ khóa `use` cho mỗi khai báo.

PHẢI có một dòng trống sau khi khối `use`.

~~~php
<?php
namespace Vendor\Package;

use FooClass;
use BarClass as Bar;
use OtherVendor\OtherPackage\BazClass;

// ... additional PHP code ...

~~~


4. Classes, Properties, and Methods
-----------------------------------

"class" bao gồm classes, interfaces, và traits.

### 4.1. Extends và Implements

`extends` và `implements` PHẢI được định nghĩa cùng dòng với class name.

- Ký tự mở class { PHẢI ở dòng tiếp theo, và đóng lớp } PHẢI ở dòng tiếp theo của thân class.


~~~php
<?php
namespace Vendor\Package;

use FooClass;
use BarClass as Bar;
use OtherVendor\OtherPackage\BazClass;

class ClassName extends ParentClass implements \ArrayAccess, \Countable
{
    // constants, properties, methods
}
~~~

Danh sách `implements` CÓ THỂ be được chia ra nhiều dòng, trong đó mỗi dòng tiếp theo được thụt lề một lần. 
Khi làm như vậy, implements đầu tiên trong danh sách phải là dòng tiếp theo, 
và phải có chỉ một interface cho mỗi dòng.

~~~php
<?php
namespace Vendor\Package;

use FooClass;
use BarClass as Bar;
use OtherVendor\OtherPackage\BazClass;

class ClassName extends ParentClass implements
    \ArrayAccess,
    \Countable,
    \Serializable
{
    // constants, properties, methods
}
~~~

### 4.2. Properties

Visibility PHẢI được khai báo trên tất cả các thuộc tính.
           
KHÔNG ĐƯỢC sử dụng từ khóa `var` để kê khai thuộc tính.

Mỗi visibility KHÔNG ĐƯỢC khai báo nhiều hơn một thuộc tính.

Tên thuộc tính KHÔNG NÊN được đặt trước bằng một dấu gạch dưới đơn để chỉ ra là private hay protected.

Khai báo thuộc tính giống như sau.

~~~php
<?php
namespace Vendor\Package;

class ClassName
{
    public $foo = null;
}
~~~

### 4.3. Methods

Visibility PHẢI được khai báo trên tất cả các methods.

Tên method KHÔNG NÊN được đặt trước bằng một dấu gạch dưới đơn để chỉ ra là private hay protected.

Tên method PHẢI KHÔNG được khai báo với dấu cách sau tên method . 
Dấu { và } PHẢI nằm trên từng dòng riêng
KHÔNG PHẢI có dấu cách sau dấu ( và  trước dấu )
Phần body PHẢI được thụt lề một lần


Một khai báo kiểu như sau. Lưu ý vị trí của ( , dấu phẩy, dấu cách và {:

~~~php
<?php
namespace Vendor\Package;

class ClassName
{
    public function fooBarBaz($arg1, &$arg2, $arg3 = [])
    {
        // method body
    }
}
~~~

### 4.4. Đối số của Method 

Trong danh sách đối số, không phải là dấu cách trước mỗi dấu phẩy, và phải là một dấu cách sau mỗi dấu phẩy.

Đối số phương pháp với giá trị mặc định PHẢI đi vào cuối danh sách đối số.

~~~php
<?php
namespace Vendor\Package;

class ClassName
{
    public function foo($arg1, &$arg2, $arg3 = [])
    {
        // method body
    }
}
~~~

Danh sách đối số có thể được chia thành nhiều dòng, trong đó mỗi dòng tiếp theo
được thụt lề một lần. Khi làm như vậy, mục đầu tiên trong danh sách PHẢI ở dòng kế tiếp,
 và PHẢI chỉ có một đối số trên mỗi dòng.

Khi danh sách các đối số được chia thành nhiều dòng, thì dấu ) và { phải được đặt cùng nhau
 trên một dòng với một khoảng trống giữa chúng.

~~~php
<?php
namespace Vendor\Package;

class ClassName
{
    public function aVeryLongMethodName(
        ClassTypeHint $arg1,
        &$arg2,
        array $arg3 = []
    ) {
        // method body
    }
}
~~~

### 4.5. `abstract`, `final`, and `static`

Khi sử dụng, `abstract` và `final` phải đứng trước khai báo visibility.

Khi sử dụng,  `static` phải đứng sau khai báo visibility.

~~~php
<?php
namespace Vendor\Package;

abstract class ClassName
{
    protected static $foo;

    abstract protected function zim();

    final public static function bar()
    {
        // method body
    }
}
~~~

### 4.6. Cách gọi Method và Function

Khi thực hiện gọi một method hoặc function, không phải có space giữa tên và dấu ngoặc mở đầu, 
KHÔNG PHẢI có space sau dấu { và không phải space trước dấu }.
 Trong danh sách đối số, KHÔNG phải có dấu cách trước mỗi dấu phẩy, và PHẢI là một dấu cách sau mỗi dấu phẩy.
 
~~~php
<?php
bar();
$foo->bar($arg1);
Foo::bar($arg2, $arg3);
~~~

Danh sách đối số có thể được chia ra nhiều dòng, trong đó mỗi dòng tiếp theo Được 
thụt lề một lần. Khi làm như vậy, mục đầu tiên trong danh sách phải ở trên dòng tiếp theo,
 và PHẢI chỉ có một đối số trên mỗi dòng.

~~~php
<?php
$foo->bar(
    $longArgument,
    $longerArgument,
    $muchLongerArgument
);
~~~

5.  Các cấu trúc điều khiển
---------------------

The general style rules for control structures are as follows:

- There MUST be one space after the control structure keyword
- There MUST NOT be a space after the opening parenthesis
- There MUST NOT be a space before the closing parenthesis
- There MUST be one space between the closing parenthesis and the opening
  brace
- The structure body MUST be indented once
- The closing brace MUST be on the next line after the body

The body of each structure MUST be enclosed by braces. This standardizes how
the structures look, and reduces the likelihood of introducing errors as new
lines get added to the body.
Các quy tắc chung cho các cấu trúc điều khiển như sau:

- PHẢI được một khoảng trắng sau từ khoá cấu trúc điều khiển
- KHÔNG PHẢI là space sau khi mở ngoặc
- KHÔNG PHẢI là space trước dấu ngoặc đóng
- Phải có một khoảng cách giữa dấu ) và {
- Body cấu trúc phải được thụt lề một lần
- Nẹp đóng } phải nằm trên dòng sau

Body của mỗi cấu trúc phải được bao quanh bởi { }. Điều này tiêu chuẩn hoá cách các
 view cấu trúc , làm giảm khả năng giới thiệu lỗi  khi các dòng mới được thêm vào body.



### 5.1. `if`, `elseif`, `else`

Cấu trúc `if` trông giống như sau. Lưu ý các vị trí của dấu ngoặc đơn, space, và {;

Và `else` và` elseif` khác cũng giống như dấu đóng của body trước đó.

~~~php
<?php
if ($expr1) {
    // if body
} elseif ($expr2) {
    // elseif body
} else {
    // else body;
}
~~~

`elseif` NÊN được sử dụng thay vì `else if`  để tất cả các từ khoá kiểm soát trông giống
 như các từ đơn lẻ.


### 5.2. `switch`, `case`

`// no break` when fall-through is intentional in a non-empty `case` body.
Một cấu trúc `switch` trông giống như sau. Lưu ý vị trí của ( ), spaces, và {}.
 Câu lệnh `case` phải được thụt lề một lần từ `switch`,
  và từ khóa` break` (hoặc từ khóa chấm dứt khác) PHẢI Được thụt lề ở cùng mức với body.
   PHẢI có một comment như `// no break` trong một trường hợp `case` trống.
   
   
~~~php
<?php
switch ($expr) {
    case 0:
        echo 'First case, with a break';
        break;
    case 1:
        echo 'Second case, which falls through';
        // no break
    case 2:
    case 3:
    case 4:
        echo 'Third case, return instead of break';
        return;
    default:
        echo 'Default case';
        break;
}
~~~


### 5.3. `while`, `do while`

Một cấu trúc `while` trông giống như sau. Lưu ý vị trí của ( ), spaces, và {}.

~~~php
<?php
while ($expr) {
    // structure body
}
~~~

Một cấu trúc `do while` trông giống như sau. Lưu ý vị trí của ( ), spaces, và {}.

~~~php
<?php
do {
    // structure body;
} while ($expr);
~~~

### 5.4. `for`

Một cấu trúc `for` trông giống như sau. Lưu ý vị trí của ( ), spaces, và {}.


~~~php
<?php
for ($i = 0; $i < 10; $i++) {
    // for body
}
~~~

### 5.5. `foreach`

Một cấu trúc `foreach` trông giống như sau. Lưu ý vị trí của ( ), spaces, và {}.


~~~php
<?php
foreach ($iterable as $key => $value) {
    // foreach body
}
~~~

### 5.6. `try`, `catch`

Một cấu trúc `try catch` trông giống như sau. Lưu ý vị trí của ( ), spaces, và {}.


~~~php
<?php
try {
    // try body
} catch (FirstExceptionType $e) {
    // catch body
} catch (OtherExceptionType $e) {
    // catch body
}
~~~

6. Closures
-----------

Closure PHẢI được khai báo với một space sau  từ khóa `function`, và một khoảng
 trống trước và sau từ khóa `use`.

Dấu { phải ở cùng một dòng, và dấu } phải PHẢI ở dòng tiếp theo.

Không phải có dấu cách sau dấu ( của list đối số hoặc danh sách biến,
 và KHÔNG PHẢI là một khoảng trắng trước dấu ) của list đối số hoặc danh sách biến.

Trong danh sách đối số và danh sách biến, KHÔNG phải có dấu cách trước mỗi  ấu phẩy,
 và phải có một dấu cách sau mỗi dấu phẩy.

đối số cua Closure với các giá trị mặc định PHẢI ở cuối danh sách đối số.

Một cấu trúc `closure` trông giống như sau. Lưu ý vị trí của ( ), spaces, và {}.


~~~php
<?php
$closureWithArgs = function ($arg1, $arg2) {
    // body
};

$closureWithArgsAndVars = function ($arg1, $arg2) use ($var1, $var2) {
    // body
};
~~~

Danh sách đối số và danh sách biến có thể được chia ra nhiều dòng, trong đó mỗi
dòng tiếp theo được thụt lề một lần. Khi làm như vậy, mục đầu tiên trong danh sách
PHẢI ở dòng kế tiếp, và PHẢI chỉ là một đối số hoặc biến mỗi dòng.

Khi danh sách kết thúc (cho dù các đối số hoặc các biến) được phân chia qua nhiều dòng,
 thì ( ) ngoặc phải được đặt cùng nhau trên một dòng với một khoảng trống giữa chúng.

Sau đây là các ví dụ về closures có và không có danh sách đối số và danh sách biến được chia thành nhiều dòng.


~~~php
<?php
$longArgs_noVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) {
    // body
};

$noArgs_longVars = function () use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
    // body
};

$longArgs_longVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
    // body
};

$longArgs_shortVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) use ($var1) {
    // body
};

$shortArgs_longVars = function ($arg) use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
    // body
};
~~~

Lưu ý rằng các quy tắc định dạng cũng được áp dụng khi closure
 được sử dụng trực tiếp trong một cuộc gọi hàm hoặc phương thức như một đối số.

~~~php
<?php
$foo->bar(
    $arg1,
    function ($arg2) use ($var1) {
        // body
    },
    $arg3
);
~~~


7. Phần kết luận
--------------

Có rất nhiều yếu tố của phong cách và thực hành cố ý bỏ qua hướng dẫn này. Chúng bao gồm:

- Declaration các biến global và hằng số toàn cục

- khai báo functions

- Operators and assignment

- Liên kết thẳng hàng

- Các khối Comments and documentation blocks

- Class name prefixes and suffixes

- Best practices
