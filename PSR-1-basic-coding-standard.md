Tiêu chuẩn code cơ b
=====================

Phần này của tiêu chuẩn bao gồm những gì cần được xem xét các thành phần code tiêu chuẩn 
được yêu cầu để đảm bảo khả năng tương tác kỹ thuật cao giữa code PHP đã được chia sẻ.

Những từ khóa "PHẢI", "PHẢI KHÔNG", "BẮT BUỘC", "KHÔNG NÊN",
, "GỢI Ý", "CÓ THỂ", và "OPTIONAL"  trong tài liệu này được giải thích
như mô tả trong [RFC 2119].

[RFC 2119]: http://www.ietf.org/rfc/rfc2119.txt
[PSR-0]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
[PSR-4]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md


1. Khái quát chung
-----------

- Files BẮT BUỘC chỉ được sử dụng thẻ `<?php` and `<?=` tags.

- Files BẮT BUỘC  chỉ được sử dụng UTF-8 mà không có BOM dành cho code PHP.

* xem them ve BOM https://jacklua.wordpress.com/2016/03/22/encoding-utf-8-va-utf-8-without-bom/

- Files NÊN khai báo các ký hiệu (classes, functions, constants, v.v..)
  * HOẶC * tạo ra các phản ứng phụ (ví dụ: tạo output, thay đổi file .ini, v.v..)
  Nhưng KHÔNG NÊN làm cả hai.

- Namespaces và classes PHẢI tuân theo một "autoloading" PSR: [[PSR-0], [PSR-4]].

- Class names PHẢI được khai báo trong `StudlyCaps`.

- Class constants PHẢI được khai báo bằng chữ in hoa và cách bởi các dấu gạch dưới (_).

- Method names PHẢI được khai báo trong `camelCase`.


2. Files
--------

### 2.1. PHP Tags

PHP code PHẢI sử dụng các thẻ `<?php ?>` hoặc `<?= ?>` ; 
KHÔNG ĐƯỢC sử dụng các biến thể thẻ khác.

### 2.2. Character Encoding

PHP code CHỈ ĐƯỢC sử dụng  UTF-8 mà không có BOM.

### 2.3. Side Effects

Một file NÊN khai báo các ký hiệu mới (classes, functions, constants, v.v..)
và không gây ra các Side Effects khác, hoặc thực hiện logic với Side Effects
, nhưng KHÔNG NÊN làm cả hai.

Cụm từ "side effects" có nghĩa là thực hiện logic không liên quan trực tiếp đến
classes, functions, constants, vv được khai báo từ file.

"Side effects" bao gồm : generating output(render,echo view), sử dụng `require` hoặc` include`,
kết nối với external services, sửa đổi ini settings, báo errors hoặc exceptions,
sửa đổi các biến global hoặc static, Đọc từ hoặc viết vào một file, v.v..

The following is an example of a file with both declarations and side effects;
i.e, an example of what to avoid:
Sau đây là một ví dụ về một file với cả khai báo và side effects;
Tức là một ví dụ về những gì cần tránh:

~~~php
<?php
// side effect: change ini settings
ini_set('error_reporting', E_ALL);

// side effect: loads a file
include "file.php";

// side effect: generates output
echo "<html>\n";

// declaration
function foo()
{
    // function body
}
~~~

Ví dụ sau là của một file có chứa các khai báo mà không có side
effects Tức là một ví dụ về những gì để mô phỏng:

~~~php
<?php
// declaration
function foo()
{
    // function body
}

// conditional declaration is *not* a side effect
if (! function_exists('bar')) {
    function bar()
    {
        // function body
    }
}
~~~


3. Namespace và Class Names
----------------------------

Namespaces và classes PHẢI tuân theo "autoloading" PSR: [[PSR-0], [PSR-4]].

Điều này có nghĩa là mỗi class nằm trong một file của chính nó, và nằm trong namespace ít nhất một cấp:
 tên một vendor cao nhất.

Tên class phải được khai báo trong `StudlyCaps`.

Code được viết cho PHP 5.3 trở lên PHẢI sử dụng namespaces hợp lệ.

Ví dụ:

~~~php
<?php
// PHP 5.3 trở lên
namespace Vendor\Model;

class Foo
{
}
~~~

Code  được viết cho 5.2.x trở xuống NÊN sử dụng quy ước pseudo-namespacing
của tiền tố `Vendor_` prefixes trên class names.

~~~php
<?php
// PHP 5.2.x trở xuống:
class Vendor_Model_Foo
{
}
~~~

4. Class Constants, Properties, và Methods
-------------------------------------------

Thuật ngữ "class" dùng để chỉ tất cả các classes, interfaces, and traits.

### 4.1. Constants

Class constants PHẢI được khai báo toàn bộ bằng chữ in hoa và dấu gạch dưới.
Ví dụ:

~~~php
<?php
namespace Vendor\Model;

class Foo
{
    const VERSION = '1.0';
    const DATE_APPROVED = '2012-06-01';
}
~~~

### 4.2. Properties

Hướng dẫn này cố ý tránh bất kỳ gợi ý nào liên quan đến việc sử dụng
property names `$ StudlyCaps`,` $ camelCase`, hoặc `$ under_score`.

Bất cứ quy ước đặt tên nào được sử dụng NÊN được áp dụng nhất quán trong phạm vi
hợp lý. Phạm vi đó có thể là mức độ vendor-level, package-level, class-level,
hoặc method-level.

### 4.3. Methods

Tên phương thức phải được khai báo trong `camelCase ()`.