Logger Interface
================

Tài liệu này mô tả một giao diện chung cho các thư viện log.

Mục đích chính là cho phép các thư viện nhận được đối tượng `Psr \ Log \ LoggerInterface`
và ghi log một cách đơn giản và bao quát. Các Frameworks và CMS có nhu cầu riêng có thể 
CÓ THỂ mở rộng interface cho mục đích riêng nhưng vẫn NÊN tương thích với tài liệu này 
để đảm bảo rằng các thư viện của bên thứ ba mà một ứng dụng sử dụng có thể ghi log vào 
nhật ký ứng dụng tập trung.


Những từ khóa "PHẢI", "PHẢI KHÔNG", "BẮT BUỘC", "KHÔNG NÊN",
, "GỢI Ý", "CÓ THỂ", và "OPTIONAL"  trong tài liệu này được giải thích
như mô tả trong [RFC 2119].

The word `implementor` in this document is to be interpreted as someone
implementing the `LoggerInterface` in a log-related library or framework.
Users of loggers are referred to as `user`.

Từ `implementor` trong tài liệu này được hiểu là người thực hiện` LoggerInterface` 
trong một thư viện hoặc Frameworks liên quan đến log. 
Người dùng logger được gọi là `user`.

[RFC 2119]: http://tools.ietf.org/html/rfc2119

1. Đặc điểm kỹ thuật
-----------------

### 1.1  Khái niệm cơ bản

- The `LoggerInterface` exposes eight methods to write logs to the eight
  [RFC 5424][] levels (debug, info, notice, warning, error, critical, alert,
  emergency).
- LoggerInterface đưa ra 8 phương pháp để viết các bản ghi tới 8 cấp [RFC 5424]
    [debug, info, notice, warning, error, critical, alert, emergency]
    (tương đương với [gỡ lỗi, thông tin, thông báo, cảnh báo, lỗi, báo động, khẩn cấp]).
   
- A ninth method, `log`, accepts a log level as the first argument. Calling this
  method with one of the log level constants MUST have the same result as
  calling the level-specific method. Calling this method with a level not
  defined by this specification MUST throw a `Psr\Log\InvalidArgumentException`
  if the implementation does not know about the level. Users SHOULD NOT use a
  custom level without knowing for sure the current implementation supports it.

- Một phương pháp thứ chín, `log`, chấp nhận log level như là đối số đầu tiên. 
  Gọi phương thức này với một trong các hằng số log level PHẢI có kết quả tương tự như 
  gọi phương thức cụ thể. 
- Gọi method này với một level không được định nghĩa bởi tài liệu này PHẢI sinh ra 
  một `Psr \ Log \ InvalidArgumentException` . 
- Người dùng KHÔNG nên sử dụng một level tùy chỉnh mà không biết chắc chắn việc 
  triển khai hiện tại hỗ trợ nó hay không.
  
[RFC 5424]: http://tools.ietf.org/html/rfc5424

### 1.2 Thông báo 

- Mỗi method chấp nhận một chuỗi ký tự như là một thông báo, hoặc một đối tượng với một
`__toString ()`. 

- Thông báo có thể có chứa các phần giữ chỗ mà lập trình viên CÓ THỂ thay thế bằng 
  các giá trị tùy trường họp cụ thể.

  Tên phần giữ chỗ PHẢI tương ứng với các key trong mảng context.

  Tên phần giữ chỗ PHẢI được phân cách bằng một dấu `{` và một dấu `}`. KHÔNG PHẢI có
  bất kỳ khoảng trắng giữa các dấu phân cách và tên phần giữ chỗ.  
  
  Tên phần giữ chỗ NÊN chỉ bao gồm các ký tự `A-Z`,` a-z`, `0-9`, gạch dưới `_`, và 
  dấu chấm `.`. Việc sử dụng các ký tự khác được dành riêng cho các sửa đổi trong tương lai
  của phần giữ chỗ đặc biệt.
  
  Lập trình viên có thể sử dụng phần giữ chỗ để thể hiện các cách giải quyết khác nhau
  và dịch log để hiển thị.

  Sau đây là một ví dụ về việc thực hiện các phần giữ chỗ để tham khảo:

  ~~~php
  <?php
 
  /**
   * Interpolates context values into the message placeholders.
   */
  function interpolate($message, array $context = array())
  {
      // build a replacement array with braces around the context keys
      $replace = array();
      foreach ($context as $key => $val) {
          // check that the value can be casted to string
          if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
              $replace['{' . $key . '}'] = $val;
          }
      }

      // interpolate replacement values into the message and return
      return strtr($message, $replace);
  }

  // a message with brace-delimited placeholder names
  $message = "User {username} created";

  // a context array of placeholder names => replacement values
  $context = array('username' => 'bolivar');

  // echoes "User bolivar created"
  echo interpolate($message, $context);
  ~~~

### 1.3 Context
- Mỗi method chấp nhận một mảng như là dữ liệu ngữ cảnh để giữ bất kỳ thông tin 
  không liên quan mà không phù hợp với một chuỗi. Mảng có thể chứa bất cứ gì. 
  Lập trình viên PHẢI đảm bảo xử lý dữ liệu theo ngữ cảnh càng rõ r càng tốt.
  Một giá trị nhất định trong ngữ cảnh PHẢI KHÔNG sinh ra một exception hoặc 
  bất kỳ lỗi, cảnh báo hoặc chú ý nào của PHP.

- Nếu một `Exception` được thông qua trong dữ liệu ngữ cảnh, nó PHẢI nằm trong 
  `` exception`` key. Trường hợp lưu trữ Exception là một loại phổ biến, cho 
  phép lập trình viên để trích xuất một stack trace từ exception khi log có hỗ 
  trợ nó. Lập trình viên PHẢI xác minh rằng từ khóa `exception` thực sự là một
  `Exception` trước khi sử dụng nó, vì nó CÓ THỂ chứa bất cứ thứ gì.

### 1.4 Helper classes and interfaces

- Class `Psr\Log\AbstractLogger` cho phép thay đổi `LoggerInterface` dễ dàng bằn
  cách mở rộng và implement method `log` thông thường.
  Có 8 methods để thông báo và context.

- Tương tự, `Psr\Log\LoggerTrait` chỉ yêu cầu implement method `log` thông thường.
  chú ý rằng traits không thể implement các interfaces khác, trong trường hợp này
  vẫn cần implement `LoggerInterface`.

- `Psr\Log\NullLogger` được sinh ra cùng với interface. Nó CÓ THỂ được sử dụng để 
  tạo thành "lỗ đen" dự trữ nếu không có log nào khác. Tuy nhiên, điều kiện ghi log 
  có thể xuất hiện nếu việc tạo dữ liệu context quá khó khăn.
  
- `Psr\Log\LoggerAwareInterface` chỉ bao gồm 1 method `setLogger(LoggerInterface $logger)`
  và có thể được sử dụng bởi các framework để tự động tạo các log khác.
  
- `Psr\Log\LoggerAwareTrait` trait có thể được dùng để implement các interface tương 
  đương trong bất kỳ class nào. Nó có thể được truy cập đến bằng cách viết `$this->logger`.
  
- Class `Psr\Log\LogLevel` giử các hằng số cho 8 cấp độ log.

2. Các gói(package)
----------

Các interface và class được miêu tả trong các exception liên quan
và một kết quả test để kiểm tra implement được tạo ra từ gói log.

3. `Psr\Log\LoggerInterface`
----------------------------

~~~php
<?php

namespace Psr\Log;

/**
 * Describes a logger instance
 *
 * The message MUST be a string or object implementing __toString().
 *
 * The message MAY contain placeholders in the form: {foo} where foo
 * will be replaced by the context data in key "foo".
 *
 * The context array can contain arbitrary data, the only assumption that
 * can be made by implementors is that if an Exception instance is given
 * to produce a stack trace, it MUST be in a key named "exception".
 *
 * See https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
 * for the full interface specification.
 */
interface LoggerInterface
{
    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function emergency($message, array $context = array());

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function alert($message, array $context = array());

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function critical($message, array $context = array());

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function error($message, array $context = array());

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function warning($message, array $context = array());

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function notice($message, array $context = array());

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function info($message, array $context = array());

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function debug($message, array $context = array());

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = array());
}
~~~

4. `Psr\Log\LoggerAwareInterface`
---------------------------------

~~~php
<?php

namespace Psr\Log;

/**
 * Describes a logger-aware instance
 */
interface LoggerAwareInterface
{
    /**
     * Sets a logger instance on the object
     *
     * @param LoggerInterface $logger
     * @return null
     */
    public function setLogger(LoggerInterface $logger);
}
~~~

5. `Psr\Log\LogLevel`
---------------------

~~~php
<?php

namespace Psr\Log;

/**
 * Describes log levels
 */
class LogLevel
{
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
}
~~~
